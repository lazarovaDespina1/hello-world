//
//  RecipeDetailViewController.swift
//  HelloWorld
//
//  Created by Jovan on 06.12.17.
//  Copyright © 2017 ITCROWD. All rights reserved.
//
import Foundation
import UIKit
import SnapKit

class RecipeDetailViewController: UIViewController {
  var navigationView : NavigationView!
  var imgViewRecipe: UIImageView!
  var lblPresentingTime: UILabel!
  var tableViewIngredients: UITableView!
  var txtViewRecipeTipe: UITextView!
  var tableViewInstructions: UITableView!
  var scrollView: UIScrollView!
  var pageControl: UIPageControl!
  
  var recipe: Recipe!
  var pageImages:[UIImage] = [UIImage]()
  var timer: Timer! = nil
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
    setupConstraints()
  }
  
  func setupViews() {
    self.view.backgroundColor = UIColor.lightGray
    pageImages.append(UIImage(named: recipe.image)!)
    pageImages.append(UIImage(named: "breakfast2")!)
    pageImages.append(UIImage(named: "breakfast3")!)
    
    navigationView = NavigationView()
    navigationView.lblTitle.text = recipe.name
    navigationView.btnBack.addTarget(self, action: #selector(backButton), for: .touchUpInside)

    lblPresentingTime = UILabel()
    lblPresentingTime.textAlignment = .left
    lblPresentingTime.adjustsFontSizeToFitWidth = true
    lblPresentingTime.minimumScaleFactor = 0.5
    lblPresentingTime.textColor = UIColor.black
    lblPresentingTime.numberOfLines = 2
    lblPresentingTime.text = "Time for prepare: \(recipe.prepTime)"
    
    tableViewIngredients = UITableView()
    tableViewIngredients.backgroundColor = UIColor.lightGray
    tableViewIngredients.register(PresentCellTableViewCell.self, forCellReuseIdentifier: "ingredients")
    tableViewIngredients.dataSource = self
    tableViewIngredients.delegate = self
    tableViewIngredients.tableHeaderView = createHeaderView(title: "Ingrediants")  
    tableViewIngredients.tableFooterView = UIView()
    
    tableViewInstructions = UITableView()
    tableViewInstructions.backgroundColor = UIColor.clear
    tableViewInstructions.register(PresentCellTableViewCell.self, forCellReuseIdentifier: "instructions")
    tableViewInstructions.dataSource = self
    tableViewInstructions.delegate = self
    tableViewInstructions.tableHeaderView = createHeaderView(title: "Instructions")
    tableViewInstructions.tableFooterView = UIView()
    
    txtViewRecipeTipe = UITextView()
    txtViewRecipeTipe.backgroundColor = UIColor.lightGray
    txtViewRecipeTipe.text = "Sample Recipe : \(recipe.tip)"
    
    pageControl = UIPageControl(frame: CGRect(x:0,y: 0, width:200, height:50))
    pageControl.numberOfPages = pageImages.count
    pageControl.currentPage = 0
    pageControl.tintColor = UIColor.red
    pageControl.pageIndicatorTintColor = UIColor.black
    pageControl.currentPageIndicatorTintColor = UIColor.green
    
    scrollView = UIScrollView(frame: CGRect(x:0, y:0, width:self.view.frame.size.width.divided(by: 2),height: self.view.frame.size.width.divided(by: 3)))
    var frame: CGRect = CGRect(x:0, y:0, width:0, height:0)
    scrollView.delegate = self
    scrollView.isPagingEnabled = true
    scrollView.contentSize = CGSize(width:self.scrollView.frame.size.width * 3, height: self.scrollView.frame.size.height)
    for index in 0..<pageImages.count {
      frame.origin.x = CGFloat(index) * scrollView.frame.size.width
      frame.size = self.scrollView.frame.size
      
      let imgViewRecipe = UIImageView(frame: frame)
      imgViewRecipe.contentMode = .scaleAspectFit
      imgViewRecipe.image = pageImages[index]
      
      self.scrollView.addSubview(imgViewRecipe)
    }
    pageControl.addTarget(self, action: #selector(self.changePage(sender:)), for: UIControlEvents.valueChanged)
    timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
    
    self.view.addSubview(scrollView)
    self.view.addSubview(navigationView)
    self.view.addSubview(pageControl)
    self.view.addSubview(lblPresentingTime)
    self.view.addSubview(tableViewIngredients)
    self.view.addSubview(tableViewInstructions)
    self.view.addSubview(txtViewRecipeTipe)
  }
  
  func setupConstraints() {
    navigationView.snp.makeConstraints{ make in
      make.top.equalTo(self.view)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.height.equalTo(self.view).dividedBy(13)
    }
    
    scrollView.snp.makeConstraints{ make in
      make.top.equalTo(navigationView.snp.bottom)
      make.left.equalTo(self.view)
      make.height.width.equalTo(self.view.snp.width).dividedBy(2)
    }
    
    pageControl.snp.makeConstraints{ make in
      make.top.equalTo(scrollView.snp.bottom)
      make.left.equalTo(self.view)
      make.centerX.equalTo(scrollView.snp.centerX)
    }
    
    lblPresentingTime.snp.makeConstraints{ make in
      make.left.equalTo(scrollView.snp.right).offset(10)
      make.right.equalTo(self.view)
      make.top.equalTo(navigationView.snp.bottom)
      make.height.equalTo(scrollView.snp.height).dividedBy(2)
    }
    
    txtViewRecipeTipe.snp.makeConstraints{ make in
      make.left.equalTo(scrollView.snp.right).offset(10)
      make.right.equalTo(self.view)
      make.top.equalTo(lblPresentingTime.snp.bottom)
      make.height.equalTo(scrollView.snp.height).dividedBy(2)
    }
    
    tableViewInstructions.snp.makeConstraints{ make in
      make.top.equalTo(scrollView.snp.bottom).offset(40)
      make.bottom.equalTo(self.view)
      make.left.equalTo(self.view)
      make.width.equalTo(self.view).dividedBy(2)
    }
    
    tableViewIngredients.snp.makeConstraints{ make in
      make.width.equalTo(self.view).dividedBy(2)
      make.top.equalTo(txtViewRecipeTipe.snp.bottom).offset(10)
      make.left.equalTo(tableViewInstructions.snp.right).offset(10)
      make.bottom.equalTo(self.view)
    }
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(false) 
    timer.invalidate()
  }
  
  func moveToNextPage() {
    let pageWidth:CGFloat = self.scrollView.frame.width
    let maxWidth:CGFloat = pageWidth * 3
    let contentOffset:CGFloat = self.scrollView.contentOffset.x
    var pageNumber: CGFloat
    var slideToX = contentOffset + pageWidth
    
    pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width) + 1
    if  contentOffset + pageWidth == maxWidth {
      slideToX = 0
      pageNumber = 0
    }
    pageControl.currentPage = Int(pageNumber)
    self.scrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.scrollView.frame.height.divided(by: 2)), animated: true)
  }
 
  func changePage(sender: AnyObject) -> () {
    let x = CGFloat(pageControl.currentPage) * scrollView.frame.size.width
    scrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
  }

  func backButton() {
    self.navigationController?.popViewController(animated: true)
  }
  
  func createHeaderView(title: String) -> UIView {
    let labelView: UILabel = UILabel()
    labelView.textColor = UIColor.black
    labelView.font = UIFont.boldSystemFont(ofSize: labelView.font.pointSize)
    labelView.textAlignment = .center
    labelView.backgroundColor = UIColor.white
    labelView.text = title
    labelView.frame = CGRect(x: 0, y: 0, width: 0, height: 50)
    return labelView
  }
}

extension RecipeDetailViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tableView == self.tableViewIngredients {
      return recipe.ingredients.count
    } else {
      return recipe.instructions.count
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if tableView == self.tableViewIngredients {
      let cell = tableView.dequeueReusableCell(withIdentifier: "ingredients", for: indexPath) as! PresentCellTableViewCell
      cell.setupCell(title: recipe.ingredients[indexPath.row])
      return cell
    } else {
      let cell = tableView.dequeueReusableCell(withIdentifier: "instructions", for: indexPath) as! PresentCellTableViewCell
      cell.setupCell(title: recipe.instructions[indexPath.row])
      return cell
    }
  }
  
}

extension RecipeDetailViewController: UIScrollViewDelegate {
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
    pageControl.currentPage = Int(pageNumber)
  }
}
