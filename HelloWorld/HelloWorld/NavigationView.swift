//
//  NavigationView.swift
//  HelloWorld
//
//  Created by Jovan on 05.12.17.
//  Copyright © 2017 ITCROWD. All rights reserved.
//

import UIKit

class NavigationView: UIView {
  var btnBack : UIButton!
  var lblTitle : UILabel!
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setupViews() {
    self.backgroundColor = UIColor.lightGray
    
    lblTitle = UILabel()
    lblTitle.text = "Navigation"
    lblTitle.textAlignment = .center
    lblTitle.textColor = UIColor.black
    lblTitle.adjustsFontSizeToFitWidth = true
    lblTitle.minimumScaleFactor = 0.5
    lblTitle.numberOfLines = 2
    
    btnBack = UIButton()
    btnBack.setTitle("Back", for: .normal)
    btnBack.setTitleColor(UIColor.black, for: .normal)
    btnBack.titleLabel?.textAlignment = .center
    
    self.addSubview(lblTitle)
    self.addSubview(btnBack)
  }
  
  func setupConstraints() {
    lblTitle.snp.makeConstraints{ make in
      make.centerY.equalTo(self).offset(10)
      make.centerX.equalTo(self)
      make.height.equalTo(40)
      make.width.equalTo(self.snp.width).dividedBy(2)
    }
    
    btnBack.snp.makeConstraints { make in
      make.left.equalTo(self).offset(20)
      make.centerY.equalTo(lblTitle.snp.centerY)
      make.width.height.equalTo(45)
    }
  }
}
