//
//  CoreDataHandler.swift
//  HelloWorld
//
//  Created by Jovan on 12.12.17.
//  Copyright © 2017 ITCROWD. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications

class CoreDataHandler: NSObject {
  func fetchObject() -> [Recipee]? {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    
    var rec:[Recipee]? = nil
  //  var recive:[Recipee]? = nil
    
    do {
      rec = try context.fetch(Recipee.fetchRequest())
//      for r in recive! {
//        if r.category == Int32(category)
//        {
//          rec?.append(r)
//        }
//      }
      return rec
    } catch {
      return rec
    }
  }
  
  func save(recipe: Recipe) {
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      return
    }
    let managedContext = appDelegate.persistentContainer.viewContext
    let entity = NSEntityDescription.entity(forEntityName: "Recipee", in: managedContext)!
    let rec = NSManagedObject(entity: entity, insertInto: managedContext)
    
    rec.setValue(recipe.name, forKeyPath: "name")
    rec.setValue(recipe.prepTime, forKeyPath: "prepTime")
    rec.setValue(recipe.image, forKeyPath: "image")
    rec.setValue(recipe.category, forKeyPath: "category")
    rec.setValue(recipe.tip, forKeyPath: "tip")
    rec.setValue(recipe.ingredients, forKey: "ingredients")
    rec.setValue(recipe.instructions, forKey: "instructions")
    do {
      try managedContext.save()
    } catch let error as NSError {
      print("Could not save. \(error), \(error.userInfo)")
    }
  }
}
