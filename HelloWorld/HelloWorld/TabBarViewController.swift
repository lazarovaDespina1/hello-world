//
//  TabBarTableViewController.swift
//
//
//  Created by Jovan on 11.12.17.
//
//

import UIKit

class TabBarViewController: UITabBarController {
  
  let firstViewController = HomeViewController()
  let secondViewController = HomeViewController()
  let thirdViewController = HomeViewController()
  let fourthViewController = WebViewController()
  var tabBarList: [UIViewController] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    firstViewController.tabBarItem = UITabBarItem(
      title: "Breakfast",
      image: UIImage(named: "tabBar"),
      tag: 1)
    firstViewController.category = 0
    
    secondViewController.tabBarItem = UITabBarItem(
      title: "Lunch",
      image: UIImage(named: "tabBar"),
      tag: 2)
    secondViewController.category = 1
    
    thirdViewController.tabBarItem = UITabBarItem(
      title: "Dessert",
      image: UIImage(named: "tabBar"),
      tag: 3)
    thirdViewController.category = 2
    
    fourthViewController.tabBarItem = UITabBarItem(
      title: "Web",
      image: UIImage(named: "tabBar"),
      tag: 4)
    
    tabBarList = [firstViewController, secondViewController, thirdViewController, fourthViewController]
    viewControllers = tabBarList.map { UINavigationController(rootViewController: $0)}
    navigationController?.isNavigationBarHidden = true
  }
}
