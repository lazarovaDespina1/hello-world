//
//  PresentCellTableViewCell.swift
//  HelloWorld
//
//  Created by Jovan on 07.12.17.
//  Copyright © 2017 ITCROWD. All rights reserved.
//

import UIKit

class PresentCellTableViewCell: UITableViewCell {
  var lblTitle: UILabel!
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setupViews() {
    lblTitle = UILabel()
    lblTitle.textAlignment = .left
    lblTitle.textColor = UIColor.black
    lblTitle.adjustsFontSizeToFitWidth = true
    lblTitle.minimumScaleFactor = 0.5
    lblTitle.numberOfLines = 4
    
    contentView.addSubview(lblTitle)
  }
  
  func setupConstraints() {
    lblTitle.snp.makeConstraints{ make in
      make.right.bottom.top.left.equalTo(self.contentView)
    }
  }
  
  func setupCell(title: String) {
    lblTitle?.text = title
  }
}
