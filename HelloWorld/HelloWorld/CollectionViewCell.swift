//
//  CollectionViewCell.swift
//  HelloWorld
//
//  Created by Jovan on 11.12.17.
//  Copyright © 2017 ITCROWD. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
  var lblRecipeTitle: UILabel!
  var imgViewRecipe: UIImageView!
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    contentView.backgroundColor = UIColor.lightGray
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setupViews() {
    imgViewRecipe = UIImageView()
    imgViewRecipe.contentMode = .scaleAspectFit
    
    lblRecipeTitle = UILabel()
    lblRecipeTitle.textAlignment = .center
    lblRecipeTitle.textColor = UIColor.black
    lblRecipeTitle.adjustsFontSizeToFitWidth = true
    lblRecipeTitle.minimumScaleFactor = 0.5
    lblRecipeTitle.numberOfLines = 3
    
    contentView.addSubview(imgViewRecipe)
    contentView.addSubview(lblRecipeTitle)
  }
  
  func setupConstraints() {
    imgViewRecipe.snp.makeConstraints{ make in
      make.height.width.equalTo(self.contentView.snp.width)
      make.top.equalTo(self.contentView)
      make.centerX.equalTo(self.contentView)
    }
    
    lblRecipeTitle.snp.makeConstraints{ make in
      make.right.left.bottom.equalTo(self.contentView)
      make.top.equalTo(imgViewRecipe.snp.bottom)
    }
  }
  
  func setupCell(rcpTitle: String, imgName: String) {
    lblRecipeTitle?.text = rcpTitle
    imgViewRecipe.image = UIImage(named: imgName)
  }
}
