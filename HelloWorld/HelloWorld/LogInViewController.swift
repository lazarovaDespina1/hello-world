//
//  ViewController.swift
//  HelloWorld
//
//  Created by Jovan on 04.12.17.
//  Copyright © 2017 ITCROWD. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class LogInViewController: UIViewController, UITextFieldDelegate {
  
  var btnLogin : UIButton!
  var viewHolder : UIView!
  var txtUserName : UITextField!
  var txtPassword : UITextField!
  var navigationView : NavigationView!
  
  var logedUser = User()
  let defaults: UserDefaults = UserDefaults.standard
  
  var buttonPressedCounter = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = UIColor.white
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(keyboardWillShow),
      name: NSNotification.Name.UIKeyboardWillShow,
      object: nil
    )
    setupViews()
    setupConstraints()
  }
  
  func setupViews() {
    navigationView = NavigationView()
    
    btnLogin = UIButton()
    btnLogin.setTitle("Login", for: .normal)
    btnLogin.setTitleColor(UIColor.white, for: .normal)
    btnLogin.titleLabel?.textAlignment = .center
    btnLogin.backgroundColor = UIColor.blue
    btnLogin.addTarget(self, action: #selector(loginButtonTouch), for: .touchUpInside)
    
    viewHolder = UIView()
    viewHolder.isHidden = true
    viewHolder.backgroundColor = UIColor.red
    
    txtUserName = UITextField()
    txtUserName.placeholder = "User Name"
    txtUserName.backgroundColor = UIColor.lightGray
    txtUserName.returnKeyType = .next
    txtUserName.delegate = self
    
    txtPassword = UITextField()
    txtPassword.placeholder = "Password"
    txtPassword.backgroundColor = UIColor.lightGray
    txtPassword.returnKeyType = .done
    txtPassword.delegate = self
    
    navigationView.btnBack.isHidden = true
    navigationView.lblTitle.text = "Log In"
    
    self.view.addSubview(btnLogin)
    self.view.addSubview(viewHolder)
    self.viewHolder.addSubview(txtUserName)
    self.viewHolder.addSubview(txtPassword)
    self.view.addSubview(navigationView)
  }
  
  func setupConstraints() {
    navigationView.snp.makeConstraints{ make in
      make.top.equalTo(self.view.snp.top)
      make.left.equalTo(self.view.snp.left)
      make.right.equalTo(self.view.snp.right)
      make.height.equalTo(self.view).dividedBy(13)
    }
    
    viewHolder.snp.makeConstraints{ make in
      make.top.equalTo(navigationView.snp.bottom).offset(50)
      make.centerX.equalTo(self.view.snp.centerX)
      make.height.equalTo(self.view).dividedBy(3)
      make.width.equalTo(self.view.snp.height).dividedBy(3)
    }
    
    txtPassword.snp.makeConstraints{ make in
      make.centerX.equalTo(viewHolder.snp.centerX)
      make.centerY.equalTo(viewHolder.snp.centerY).offset(20)
      make.height.equalTo(30)
      make.width.equalTo(150)
    }
    
    txtUserName.snp.makeConstraints{ make in
      make.centerX.equalTo(viewHolder.snp.centerX)
      make.centerY.equalTo(viewHolder.snp.centerY).offset(-20)
      make.height.equalTo(30)
      make.width.equalTo(150)
    }
    
    btnLogin.snp.makeConstraints{ make in
      make.bottom.equalTo(self.view.snp.bottom).offset(-50)
      make.centerX.equalTo(self.view.snp.centerX)
      make.width.equalTo(self.view).dividedBy(2)
      make.height.equalTo(50)
    }
  }
  
  func loginButtonTouch() {
    viewHolder.isHidden = false
    if buttonPressedCounter == 0 {
      self.viewHolder.alpha = 0.0
      UIView.animate(withDuration: 1.0, animations: {
        self.viewHolder.alpha = 1.0
      })
      buttonPressedCounter = 1
    } else if buttonPressedCounter == 1 {
      if txtUserName.text != "" && txtUserName.text != "" {
        defaults.set(txtUserName.text, forKey: "userName")
        defaults.set(txtUserName.text, forKey: "password")
        let home = HomeViewController()
        let tab = TabBarViewController()
        home.user.userName = logedUser.userName
        home.user.password = logedUser.password
        self.navigationController?.pushViewController(tab, animated: true)
      }
    }
  }
  
  func keyboardWillShow(notification: NSNotification) {
    if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
      let keyboardHeight = keyboardSize.height
      UIView.animate(withDuration: 3.0, animations: {
        self.btnLogin.snp.updateConstraints{ make in
          make.bottom.equalTo(self.view.snp.bottom).offset(-(keyboardHeight+10))
        }
        self.view.layoutIfNeeded()
      })
    }
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    if textField == txtUserName {
      txtPassword.becomeFirstResponder()
    } else if textField == txtPassword {
      UIView.animate(withDuration: 3.0, animations: {
        self.btnLogin.snp.updateConstraints{ make in
          make.bottom.equalTo(self.view.snp.bottom).offset(-50)
        }
        self.view.layoutIfNeeded()
      })
      self.viewHolder.alpha = 1.0
      UIView.animate(withDuration: 1.0, animations: {
        self.viewHolder.alpha = 0.0
      })
    }
    return true
  }
  
}

