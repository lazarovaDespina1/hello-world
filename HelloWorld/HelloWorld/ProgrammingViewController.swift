//
//  ProgrammingViewController.swift
//  HelloWorld
//
//  Created by Jovan on 15.12.17.
//  Copyright © 2017 ITCROWD. All rights reserved.
//

import UIKit

class ProgrammingViewController: UIViewController, UITextFieldDelegate {
  
  var firstTextField: UITextField!
  var secondTextField: UITextField!
  var resultLabel: UILabel!
  
  var firstInput: Int!
  var secondInput: Int!
  var array: [Int] = []
  
  var arrayMixed = [2,5,8,11,19,13,1,3,6,4,10,9,13,12,15,14,20,18,17,16]
  
  //var arrayMixed = [3,2,4,8,7,5,1]
  
  var numbers: [Int] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
    setupConstraints()
    
    for i in 1...50 {
      if i != 38{
        array.append(i)
      }
    }
   
    let sorted = sort(array: arrayMixed)
    print(sorted)
    print("Missing number in Sorted array: \(missingNumber(array: sorted))")
  }
  

  
  func setupViews(){
    self.view.backgroundColor = UIColor.white
    
    firstTextField = UITextField()
    firstTextField.placeholder = "First Number"
    firstTextField.backgroundColor = UIColor.lightGray
    firstTextField.returnKeyType = .next
    firstTextField.delegate = self
    
    secondTextField = UITextField()
    secondTextField.placeholder = "Second Number"
    secondTextField.backgroundColor = UIColor.lightGray
    secondTextField.returnKeyType = .done
    secondTextField.delegate = self
    
    resultLabel = UILabel()
    resultLabel.text = "Result"
    
    self.view.addSubview(firstTextField)
    self.view.addSubview(secondTextField)
    self.view.addSubview(resultLabel)
  }
  
  func setupConstraints(){
    firstTextField.snp.makeConstraints{ make in
      make.centerX.equalTo(view.self)
      make.centerY.equalTo(view.self).offset(-100)
      make.height.equalTo(30)
      make.width.equalTo(view.snp.width).offset(-40)
    }
    secondTextField.snp.makeConstraints{ make in
      make.top.equalTo(firstTextField.snp.bottom).offset(10)
      make.centerX.equalTo(view.self)
      make.height.equalTo(30)
      make.width.equalTo(view.snp.width).offset(-40)
    }
    resultLabel.snp.makeConstraints{ make in
      make.top.equalTo(secondTextField.snp.bottom).offset(40)
      make.centerX.equalTo(view.self)
      make.height.equalTo(30)
      make.width.equalTo(view.snp.width).offset(-40)
    }
  }
  
  func missingNumber(array: [Int]) -> Int {
    var missingNumber = 0
    let arr = array

    for i in array {
      if arr.count > i {
        if i > 1 {
          let a = arr[i-1]
          if a != i
          {
            missingNumber = i - 1
            return missingNumber
          }
        }
      }
    }
    return 0
  }
  
  func sort(array: [Int]) -> [Int] {
  var a = array
   for _ in 0...(a.count - 2)
   {
      for j in 0...(a.count - 2) {
        if a[j] > a[j+1] {
          let pre = a[j+1]
          a[j+1]=a[j]
          a[j] = pre
        }
      }
    }
    return a
  }
  
  func sum(from: Int, to: Int) -> Int {
    var sum = 0
    for i in from...to {
      sum = sum + i
    }
    return sum
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    if textField == firstTextField {
      secondTextField.becomeFirstResponder()
    } else if textField == secondTextField {
      firstInput = Int(firstTextField.text!)
      secondInput = Int(secondTextField.text!)
      
      var brojac = 2
      let len = (firstTextField.text?.characters.count)!
      var i = Int(pow(10, Double(len - 1)))
      
      while(i != 1)
      {
        let digit = firstInput / i
        if((digit % 2) == 0)
        {
          numbers.append(digit)
        }
       firstInput = firstInput - (i * digit)
        i = Int(pow(10, Double(len - brojac)))
        brojac = brojac + 1
      }
      
      var suma = 0
      for i in numbers {
        suma = suma + i
      }
      
      print(numbers)
      resultLabel.text = String(suma)
      

//      if firstInput < secondInput {
//       resultLabel.text = String(describing: sum(from: firstInput, to: secondInput))
//      } else {
//        resultLabel.text = "Wrong parameters"
//      }
      
    }
    return true
  }
}
