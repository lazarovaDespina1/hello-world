//
//  CustomCell.swift
//  HelloWorld
//
//  Created by Jovan on 06.12.17.
//  Copyright © 2017 ITCROWD. All rights reserved.
//

import UIKit
import SnapKit

class CustomCell: UITableViewCell {
  
  var lblRecipeTitle: UILabel!
  var imgViewRecipe: UIImageView!
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setupViews() {
    imgViewRecipe = UIImageView()
    imgViewRecipe.contentMode = .scaleAspectFit
    imageView?.backgroundColor = UIColor.red
    
    lblRecipeTitle = UILabel()
    lblRecipeTitle.textAlignment = .left
    lblRecipeTitle.textColor = UIColor.black
    
    contentView.addSubview(imgViewRecipe)
    contentView.addSubview(lblRecipeTitle)
  }
  
  func setupConstraints() {
    imgViewRecipe.snp.makeConstraints{ make in
      make.left.equalTo(self.contentView).offset(20)
      make.top.bottom.equalTo(self.contentView)
      make.height.width.equalTo(self.contentView.snp.height)
    }
    
    lblRecipeTitle.snp.makeConstraints{ make in
      make.right.bottom.top.equalTo(self.contentView)
      make.left.equalTo(imgViewRecipe.snp.right).offset(10)
    }
  }
  
  func setupCell(rcpTitle: String, imgName: String) {
    lblRecipeTitle?.text = rcpTitle
    imgViewRecipe.image = UIImage(named: imgName)
  }
}

