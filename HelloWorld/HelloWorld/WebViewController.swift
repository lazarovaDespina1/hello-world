//
//  WebViewController.swift
//  HelloWorld
//
//  Created by Jovan on 11.12.17.
//  Copyright © 2017 ITCROWD. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
  var webView: UIWebView!
  var navigationView : NavigationView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
    setupConstraints()
  }
  
  func setupViews() {
    navigationView = NavigationView()
    navigationView.lblTitle.text = "All Recipes"
    navigationView.btnBack.isHidden = true
    
    webView = UIWebView()
    let url = URL(string: "https://allrecipes.com")!
    webView.loadRequest(URLRequest(url: url))
    
    self.view.addSubview(navigationView)
    self.view.addSubview(webView)
  }
  
  func setupConstraints() {
    navigationView.snp.makeConstraints{ make in
      make.top.right.left.equalTo(self.view)
      make.height.equalTo(self.view).dividedBy(13)
    }
    
    webView.snp.makeConstraints{ make in
      make.top.equalTo(navigationView.snp.bottom)
      make.left.right.bottom.equalTo(self.view)
    }
  }
}
