//
//  HomeViewController.swift
//  HelloWorld
//
//  Created by Jovan on 05.12.17.
//  Copyright © 2017 ITCROWD. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import CoreData
import UserNotifications

class HomeViewController: UIViewController, UISearchBarDelegate{
 
  var message: UILabel!
  var userLoged: UILabel!
  var helloWorld: UIButton!
  var navigationView : NavigationView!
  var tableView: UITableView!
  var searchBar: UISearchBar!
  var collectionView: UICollectionView!
  
  var filteredRecipies = [Recipe]()
  var searchActive : Bool = false
  var resipesDataSorce = [Recipe]()
  var user = User()
  var userName: Any? = nil
  var category = 0
  let core = CoreDataHandler()
  var recipe: [Recipee] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = UIColor.white
    navigationController?.isNavigationBarHidden = true
    let login = LogInViewController()
    userName = login.defaults.object(forKey: "userName")
    setupViews()
    setupConstraints()
    fillDataSource()
  }  
  
  func fillDataSource() {
    recipe = core.fetchObject()!
    
    for r in recipe {
        resipesDataSorce.append(Recipe(name: r.name!, prepTime: r.prepTime!, image: r.image!, ingredients: r.ingredients as! [String], category: Int(r.category), instructions: r.instructions as! [String], tip:r.tip!))
      }
    
//    if category == 0 {
//      for r in recipe {
//        if r.category == 0 {
//          resipesDataSorce.append(Recipe(name: r.name!, prepTime: r.prepTime!, image: r.image!, ingredients: r.ingredients as! [String], category: Int(r.category), instructions: r.instructions as! [String], tip:r.tip!))
//        }
//      }
//    }else if category == 1 {
//      for r in recipe {
//        if r.category == 1 {
//          resipesDataSorce.append(Recipe(name: r.name!, prepTime: r.prepTime!, image: r.image!, ingredients: r.ingredients as! [String], category: Int(r.category), instructions: r.instructions as! [String], tip:r.tip!))
//        }
//      }
//    }else {
//      for r in recipe {
//        if r.category == 2 {
//          resipesDataSorce.append(Recipe(name: r.name!, prepTime: r.prepTime!, image: r.image!, ingredients: r.ingredients as! [String], category: Int(r.category), instructions: r.instructions as! [String], tip:r.tip!))
//        }
//      }
//    }
  }
  
  func setupViews() {
    navigationView = NavigationView()
    navigationView.btnBack.addTarget(self, action: #selector(backButton), for: .touchUpInside)
    navigationView.btnBack.isHidden = true
    if category == 0 {
      navigationView.lblTitle.text = "Breakfast"
    } else if category == 1 {
      navigationView.lblTitle.text = "Lunch"
    } else if category == 2 {
      navigationView.lblTitle.text = "Dessert"
    }
    
    tableView = UITableView()
    tableView.backgroundColor = UIColor.lightGray
    tableView.register(CustomCell.self, forCellReuseIdentifier: "cell")
    tableView.tableFooterView = createFooterView()
    tableView.dataSource = self
    tableView.delegate = self
    
    searchBar = UISearchBar()
    searchBar.delegate = self
    searchBar.frame = CGRect(x: 0, y: 0, width: 300, height: 50)
    searchBar.layer.position = CGPoint(x: self.view.bounds.width/2, y: 100)
    searchBar.searchBarStyle = UISearchBarStyle.default
    searchBar.placeholder = "Input text"
    
    let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    layout.itemSize = CGSize(width: (view.frame.width - 20).divided(by: 3), height: view.frame.height.divided(by: 3.5))
    layout.minimumInteritemSpacing = 10
    collectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
    collectionView.backgroundColor = UIColor.white
    collectionView.register(CollectionViewCell.self, forCellWithReuseIdentifier: "collectionCell")
    collectionView.dataSource = self
    collectionView.delegate = self
    
    self.view.addSubview(searchBar)
    self.view.addSubview(navigationView)
    if category == 2 {
      self.view.addSubview(collectionView)
    } else {
      self.view.addSubview(tableView)
    }
  }
  
  func setupConstraints() {
    navigationView.snp.makeConstraints{ make in
      make.top.equalTo(self.view.snp.top)
      make.left.equalTo(self.view.snp.left)
      make.right.equalTo(self.view.snp.right)
      make.height.equalTo(self.view).dividedBy(13)
    }
    
    searchBar.snp.makeConstraints{ make in
      make.top.equalTo(navigationView.snp.bottom)
      make.left.right.equalTo(self.view)
      make.height.equalTo(50)
    }
    
    if category == 2 {
      collectionView.snp.makeConstraints{ make in
        make.top.equalTo(searchBar.snp.bottom)
        make.bottom.equalTo(self.view).offset(-50)
        make.left.right.equalTo(self.view)
      }
    } else {
      tableView.snp.makeConstraints{ make in
        make.top.equalTo(searchBar.snp.bottom)
        make.left.right.bottom.equalTo(self.view)
      }
    }
  }
  
  func showAlertButtonTapped() {
    let alert = UIAlertController(title: helloWorld.currentTitle, message: nil, preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
    self.present(alert, animated: true, completion: nil)
  }
  
  func backButton() {
    let login = LogInViewController()
    self.navigationController?.pushViewController(login, animated: true)
  }
  
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder() // hides the keyboard.
    searchActive = false;
  }
  
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    searchActive = true;
  }
  
  func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    searchActive = false;
  }
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    filteredRecipies = resipesDataSorce.filter { $0.name.hasPrefix(searchText) }
    self.tableView.reloadData()
  }
  
  func createFooterView() -> UIView {
    let button = UIButton()
    button.setTitle("Notification", for: .normal)
    button.setTitleColor(UIColor.white, for: .normal)
    button.titleLabel?.textAlignment = .center
    button.backgroundColor = UIColor.blue
    button.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
    button.addTarget(self, action: #selector(notificationButtonTouch), for: .touchUpInside)
    return button
  }
  
  func notificationButtonTouch() {
    
    let notification = UNMutableNotificationContent()
    notification.title = "Your meal is ready"
    notification.subtitle = "meal reminder"
    notification.body = "Your meal is ready"

    if let path = Bundle.main.path(forResource: "breakfast1", ofType: "png") {
      let url = URL(fileURLWithPath: path)
      
      do {
        let attachment = try UNNotificationAttachment(identifier: "breakfast", url: url, options: nil)
        notification.attachments = [attachment]
      } catch {
        print("The attachment was not loaded.")
      }
    }
    
    notification.categoryIdentifier = "options"
    
    let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
    let request = UNNotificationRequest(identifier: "notification1", content: notification, trigger: notificationTrigger)
    let center = UNUserNotificationCenter.current()
    
    center.add(request, withCompletionHandler: { (error) in if let error = error {
      print("ERROR : \(error)")
      }
    })
  }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if(searchActive) {
      return filteredRecipies.count
    } else {
      return resipesDataSorce.count
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomCell
    if(searchActive) {
      let recipe = filteredRecipies[indexPath.row]
      cell.setupCell(rcpTitle: recipe.name, imgName: recipe.image)
      return cell
    } else {
      let recipe = resipesDataSorce[indexPath.row]
      cell.setupCell(rcpTitle: recipe.name, imgName: recipe.image)
      return cell
    }
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let destination = RecipeDetailViewController()
    let recipe = resipesDataSorce[indexPath.row]
    destination.recipe = recipe
    self.navigationController?.pushViewController(destination, animated: true)
  }
  
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
      resipesDataSorce.remove(at: indexPath.row)
      tableView.deleteRows(at: [indexPath], with: .none)
    }
  }
}

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if(searchActive) {
      return filteredRecipies.count
    } else {
      return resipesDataSorce.count
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath)as! CollectionViewCell
    if(searchActive) {
      let recipe = filteredRecipies[indexPath.row]
      cell.setupCell(rcpTitle: recipe.name, imgName: recipe.image)
      return cell
    } else {
      let recipe = resipesDataSorce[indexPath.row]
      cell.setupCell(rcpTitle: recipe.name, imgName: recipe.image)
      return cell
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let destination = RecipeDetailViewController()
    let recipe = resipesDataSorce[indexPath.row]
    destination.recipe = recipe
    self.navigationController?.pushViewController(destination, animated: true)
  }
}

