//
//  Recipe.swift
//  HelloWorld
//
//  Created by Jovan on 06.12.17.
//  Copyright © 2017 ITCROWD. All rights reserved.
//

import Foundation

class Recipe: NSObject {
  var name: String
  var prepTime: String
  var image: String
  var ingredients: [String]
  var category: Int
  var instructions: [String]
  var tip: String
  
  init(name: String, prepTime: String, image: String, ingredients: [String], category: Int, instructions: [String], tip: String) {
    self.name = name
    self.prepTime = prepTime
    self.image = image
    self.ingredients = ingredients
    self.category = category
    self.instructions = instructions
    self.tip = tip
    super.init()
  }  
}
